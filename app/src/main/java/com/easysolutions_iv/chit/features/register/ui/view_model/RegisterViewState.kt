package com.easysolutions_iv.chit.features.register.ui.view_model

import com.easysolutions_iv.chit.features.register.ui.models.RoleUI

data class RegisterViewState(
    val isProgressbarVisible: Boolean = false,
    val roles: List<RoleUI>
)