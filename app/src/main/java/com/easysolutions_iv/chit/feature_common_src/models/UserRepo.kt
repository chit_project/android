package com.easysolutions_iv.chit.feature_common_src.models

import kotlinx.serialization.Serializable

@Serializable
data class UserRepo(
    val login: String,
    val nickName: String,
    val role: String
)