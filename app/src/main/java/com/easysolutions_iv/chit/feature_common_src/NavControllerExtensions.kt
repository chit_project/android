package com.easysolutions_iv.chit.feature_common_src

import androidx.navigation.NavController
import com.easysolutions_iv.chit.MainNavigationDirections
import com.easysolutions_iv.chit.R

fun NavController.navigateToLoginScreen() {
    if (currentDestination?.id != R.id.loginFragment) {
        navigate(MainNavigationDirections.goToLogin())
    }
}