package com.easysolutions_iv.chit.features.register.domain

import com.easysolutions_iv.chit.features.auth.domain.AuthInteractor
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.easysolutions_iv.chit.app_environment.utils.Result
import com.easysolutions_iv.chit.feature_common_src.models.UserInternal

class RegisterInteractor @Inject constructor(
    private val authInteractor: AuthInteractor
) {
    suspend fun tryToRegister(userInternal: UserInternal) {
        authInteractor.tryToRegister(userInternal)
    }

    val authFlow: Flow<Result<AuthState>>
        get() = authInteractor.authFlow
}