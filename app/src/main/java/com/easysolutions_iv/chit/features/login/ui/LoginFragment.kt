package com.easysolutions_iv.chit.features.login.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.easysolutions_iv.chit.R
import com.easysolutions_iv.chit.app_environment.utils.BaseFragment
import com.easysolutions_iv.chit.app_environment.utils.extensions.safeNavigate
import com.easysolutions_iv.chit.app_environment.utils.extensions.showAlertDialog
import com.easysolutions_iv.chit.databinding.FragmentLoginBinding
import com.easysolutions_iv.chit.features.login.ui.view_model.LoginViewEvent
import com.easysolutions_iv.chit.features.login.ui.view_model.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    private val fragmentViewModel by viewModels<LoginViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            isEnabled = false
            requireActivity().finish()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.lifecycleOwner = viewLifecycleOwner
        viewBinding.viewModel = fragmentViewModel

        fragmentViewModel.viewEvent.observe(viewLifecycleOwner) { event ->
            when (event) {
                LoginViewEvent.Register -> {
                    safeNavigate(
                        R.id.loginFragment,
                        LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
                    )
                }
                LoginViewEvent.StartWorking -> {
                    safeNavigate(
                        R.id.loginFragment,
                        LoginFragmentDirections.actionLoginFragmentToStartWorkingFragment()
                    )
                }
                is LoginViewEvent.ShowAlertDialog -> showAlertDialog(event.state)
            }
        }
    }

    override fun provideBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentLoginBinding.inflate(inflater, container, false)
}