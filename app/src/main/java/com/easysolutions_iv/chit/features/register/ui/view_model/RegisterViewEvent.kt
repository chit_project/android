package com.easysolutions_iv.chit.features.register.ui.view_model

import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogState

sealed class RegisterViewEvent {

    data class ShowAlertDialog(val state: AlertDialogState) : RegisterViewEvent()

    object StartWorking : RegisterViewEvent()
}