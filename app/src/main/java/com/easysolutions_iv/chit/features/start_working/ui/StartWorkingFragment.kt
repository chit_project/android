package com.easysolutions_iv.chit.features.start_working.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.easysolutions_iv.chit.app_environment.utils.BaseFragment
import com.easysolutions_iv.chit.databinding.FragmentStartWorkingBinding
import com.easysolutions_iv.chit.feature_common_src.navigateToLoginScreen
import com.easysolutions_iv.chit.features.start_working.ui.view_model.StartWorkingViewEvent
import com.easysolutions_iv.chit.features.start_working.ui.view_model.StartWorkingViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StartWorkingFragment : BaseFragment<FragmentStartWorkingBinding>() {

    private val fragmentViewModel by viewModels<StartWorkingViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.lifecycleOwner = viewLifecycleOwner
        viewBinding.viewModel = fragmentViewModel

        fragmentViewModel.viewEvent.observe(viewLifecycleOwner) { event ->
            when (event) {
                StartWorkingViewEvent.Logout -> {
                    navController.navigateToLoginScreen()
                }
            }
        }
    }

    override fun provideBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentStartWorkingBinding.inflate(inflater, container, false)
}