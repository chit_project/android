package com.easysolutions_iv.chit.app_environment.utils.alert_dialog

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AlertDialogState(
    val fragmentTag: String,
    val title: String = "",
    val text: String = "",
    val isTextSelectable: Boolean = false,
    val isCancelable: Boolean = true,
    val positiveButtonText: String = "",
    val negativeButtonText: String = "",
    val neutralButtonText: String = "",
    val alertDialogClickListener: AlertDialogClickListener? = null
) : Parcelable