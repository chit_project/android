package com.easysolutions_iv.chit.feature_common_src.models

fun User.toUserRepo(): UserRepo = UserRepo(
    login = login,
    nickName = nickName,
    role = role.name
)

fun UserRepo.toUser(): User = User(
    login = login,
    nickName = nickName,
    role = Role[role]
)