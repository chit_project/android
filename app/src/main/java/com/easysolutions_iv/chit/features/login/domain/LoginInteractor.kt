package com.easysolutions_iv.chit.features.login.domain

import com.easysolutions_iv.chit.features.auth.domain.AuthInteractor
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.easysolutions_iv.chit.app_environment.utils.Result

class LoginInteractor @Inject constructor(
    private val authInteractor: AuthInteractor
) {

    val authFlow: Flow<Result<AuthState>>
        get() = authInteractor.authFlow

    suspend fun tryToLogin(login: String, password: String) {
        authInteractor.tryToLogin(login, password)
    }
}