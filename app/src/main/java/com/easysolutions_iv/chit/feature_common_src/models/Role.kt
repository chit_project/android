package com.easysolutions_iv.chit.feature_common_src.models

enum class Role {
    Developer, DevOps, Tester, Designer, Analyst, ProductOwner;

    companion object {

        /** Получить роль по типу */
        operator fun get(type: String): Role =
            values().find { it.name == type }
                ?: throw java.lang.IllegalStateException("Unknown role")
    }
}