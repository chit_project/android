package com.easysolutions_iv.chit.features.login.ui.view_model

import android.content.res.Resources
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.*
import com.easysolutions_iv.chit.R
import com.easysolutions_iv.chit.app_environment.utils.Result
import com.easysolutions_iv.chit.app_environment.utils.SingleLiveEvent
import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogState
import com.easysolutions_iv.chit.app_environment.utils.exceptions.WrongCredentialsException
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import com.easysolutions_iv.chit.features.login.domain.LoginInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val interactor: LoginInteractor,
    private val resources: Resources
) : ViewModel() {

    private var login: String = ""
    private var password: String = ""

    private val _viewEvent = SingleLiveEvent<LoginViewEvent>()
    val viewEvent: LiveData<LoginViewEvent> = _viewEvent

    val viewState = liveData(viewModelScope.coroutineContext) {
        emit(LoginViewState())
        emitSource(createViewStateLiveData())
    }.distinctUntilChanged()

    val loginWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(editable: Editable?) {
            login = editable?.toString() ?: ""
        }
    }

    val passwordWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(editable: Editable?) {
            password = editable?.toString() ?: ""
        }
    }

    private fun LiveDataScope<LoginViewState>.createViewStateLiveData() =
        interactor.authFlow.map { authStateResult ->
            val viewState: LoginViewState = requireNotNull(latestValue)
            when (authStateResult) {
                Result.Loading -> {
                    viewState.copy(isProgressbarVisible = true)
                }
                is Result.Error -> {
                    val text = if (authStateResult.exception is WrongCredentialsException) {
                        R.string.error_credentials.text()
                    } else {
                        R.string.error_network.text()
                    }
                    val state = AlertDialogState(
                        fragmentTag = "Error",
                        title = R.string.error.text(),
                        text = text,
                        positiveButtonText = resources.getString(android.R.string.ok)
                    )
                    _viewEvent.value = LoginViewEvent.ShowAlertDialog(state)
                    viewState.copy(isProgressbarVisible = false)
                }
                is Result.Success -> {
                    when (authStateResult.data) {
                        is AuthState.Authorized -> {
                            _viewEvent.value = LoginViewEvent.StartWorking
                            viewState.copy(isProgressbarVisible = false)
                        }
                        else -> viewState
                    }
                }
            }
        }.asLiveData(viewModelScope.coroutineContext)

    fun signIn() {
        val isAllFieldsFilled = login.isNotBlank() && password.isNotBlank()
        if (isAllFieldsFilled) {
            viewModelScope.launch {
                interactor.tryToLogin(login, password)
            }
        } else {
            val state = AlertDialogState(
                fragmentTag = "LoginViewModel: Not all fields filled",
                title = R.string.error.text(),
                text = R.string.error_not_all_fields_filled.text(),
                positiveButtonText = resources.getString(android.R.string.ok)
            )
            _viewEvent.value = LoginViewEvent.ShowAlertDialog(state)
        }
    }

    fun register() {
        _viewEvent.value = LoginViewEvent.Register
    }

    private fun Int.text() = resources.getString(this)
}