package com.easysolutions_iv.chit.features.start_working.ui.view_model

data class StartWorkingViewState(
    val greetings: String = "",
    val role: String = ""
)