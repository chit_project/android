package com.easysolutions_iv.chit.app_environment.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.easysolutions_iv.chit.R
import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogFragment
import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogManager
import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogState
import com.easysolutions_iv.chit.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), AlertDialogManager {

    private lateinit var binding: ActivityMainBinding
    private val navController: NavController
        get() {
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
            return navHostFragment.navController
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater).apply { setContentView(root) }
    }

    override fun showAlertDialog(dialogState: AlertDialogState) {
        AlertDialogFragment.show(supportFragmentManager, dialogState)
    }

    override fun hideDialogByTag(fragmentTag: String) {
        supportFragmentManager.findFragmentByTag(fragmentTag)?.let { fragment ->
            (fragment as? AlertDialogFragment)?.dismiss()
        }
    }

    override fun onBackPressed() {
        if (navController.currentDestination?.id != R.id.startWorkingFragment) super.onBackPressed()
    }
}