@file:Suppress("unused")

package com.easysolutions_iv.chit.app_environment.utils

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Класс-расширение LiveData для единоразовых событий (типа тостов и т.п.).
 * Если событие было обработано, то при следующей подписке событие не оправится к подписчику
 */
class SingleLiveEvent<T: Any?> : MutableLiveData<T>() {

    private val pending = AtomicBoolean(false)

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        super.observe(owner) {
            if (pending.compareAndSet(true, false)) observer.onChanged(it)
        }
    }

    @MainThread
    override fun setValue(value: T?) {
        pending.set(true)
        super.setValue(value)
    }

    override fun postValue(value: T) {
        pending.set(true)
        super.postValue(value)
    }

    /** Used for cases where T is Void, to make calls cleaner */
    @MainThread
    fun call() {
        value = null
    }
}