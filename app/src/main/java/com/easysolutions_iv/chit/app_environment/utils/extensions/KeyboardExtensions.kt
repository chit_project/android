package com.easysolutions_iv.chit.app_environment.utils.extensions

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.WindowInsets
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.core.os.postDelayed
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.doOnLayout
import androidx.fragment.app.Fragment

/** @SelfDocumented */
val View.isKeyboardVisible: Boolean
    get() = ViewCompat.getRootWindowInsets(this)?.isVisible(WindowInsetsCompat.Type.ime()) ?: false

/** Показать/скрыть клавиатуру */
fun View.setKeyboardVisibility(boolean: Boolean) {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    if (boolean) {
        Handler(Looper.getMainLooper()).postDelayed(300) {
            if (requestFocus()) {
                isFocusableInTouchMode = true
                inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_FORCED)
            }
        }
    } else {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
        clearFocus()
    }
}

/** @SelfDocumented */
fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

/** @SelfDocumented */
fun Activity.hideKeyboard() {
    if (currentFocus == null) {
        View(this).setKeyboardVisibility(false)
    } else {
        currentFocus?.let { hideKeyboard(it) }
    }
}

/** @SelfDocumented */
fun Fragment.hideKeyboard() {
    requireActivity().hideKeyboard()
}

@RequiresApi(Build.VERSION_CODES.R)
fun View.addKeyboardVisibilityListener(keyboardCallback: (visible: Boolean) -> Unit) {
    doOnLayout {
        //get init state of keyboard
        var keyboardVisible = rootWindowInsets?.isVisible(WindowInsets.Type.ime()) == true

        //callback as soon as the layout is set with whether the keyboard is open or not
        keyboardCallback(keyboardVisible)

        //whenever there is an inset change on the App, check if the keyboard is visible.
        setOnApplyWindowInsetsListener { _, windowInsets ->
            val keyboardUpdateCheck =
                rootWindowInsets?.isVisible(WindowInsets.Type.ime()) == true
            //since the observer is hit quite often, only callback when there is a change.
            if (keyboardUpdateCheck != keyboardVisible) {
                keyboardCallback(keyboardUpdateCheck)
                keyboardVisible = keyboardUpdateCheck
            }

            windowInsets
        }
    }
}