package com.easysolutions_iv.chit.features.auth.repository

import com.easysolutions_iv.chit.app_environment.utils.Result
import com.easysolutions_iv.chit.app_environment.utils.SafeApiRepository
import com.easysolutions_iv.chit.app_environment.utils.exceptions.DuplicateUserException
import com.easysolutions_iv.chit.app_environment.utils.exceptions.UnknownException
import com.easysolutions_iv.chit.app_environment.utils.exceptions.WrongCredentialsException
import com.easysolutions_iv.chit.feature_common_src.UserApi
import com.easysolutions_iv.chit.feature_common_src.models.UserInternal
import com.easysolutions_iv.chit.feature_common_src.models.toUser
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import timber.log.Timber
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val userRequest: UserApi,
    private val safeApiRepository: SafeApiRepository
) : AuthRepository {

    private val _authFlow: MutableStateFlow<Result<AuthState>> =
        MutableStateFlow(Result.Success(AuthState.NotDefined))
    override val authFlow: Flow<Result<AuthState>>
        get() = _authFlow.asStateFlow()

    override suspend fun tryToLogin(login: String, password: String) {
        val prevState = _authFlow.value

        _authFlow.update { Result.Loading }

        val apiResult = safeApiRepository.safeApiCall {
            userRequest.getUserBy(login, password)
        }

        when (apiResult) {

            is Result.Error -> {
                _authFlow.update {
                    Timber.e(apiResult.exception, "Some network exception.")
                    Result.Error(apiResult.exception)
                }
                _authFlow.update { prevState }
            }

            Result.Loading -> {
                _authFlow.update {
                    Result.Loading
                }
            }

            is Result.Success -> {
                val userRepo = apiResult.data
                val isUserExist = userRepo.login.isNotBlank()
                if (isUserExist) {
                    _authFlow.update {
                        Result.Success(AuthState.Authorized(userRepo.toUser()))
                    }
                } else {
                    _authFlow.update {
                        Result.Error(WrongCredentialsException())
                    }
                    _authFlow.update { prevState }
                }
            }
        }
    }

    override suspend fun tryToRegister(userRepoInternal: UserInternal) {
        val prevState = _authFlow.value

        _authFlow.update { Result.Loading }

        val apiResult = safeApiRepository.safeApiCall {
            userRequest.postUser(userRepoInternal)
        }

        when (apiResult) {

            is Result.Error -> {
                _authFlow.update {
                    if (apiResult.exception.message?.contains(USER_EXISTS_ERROR_STATUS) == true) {
                        val exception = DuplicateUserException()
                        Timber.e(exception, "Duplicate user exception.")
                        Result.Error(exception)
                    } else {
                        Timber.e(apiResult.exception, "Some network exception.")
                        Result.Error(apiResult.exception)
                    }
                }
                _authFlow.update { prevState }
            }

            Result.Loading -> {
                _authFlow.update {
                    Result.Loading
                }
            }

            is Result.Success -> {
                val userRepo = apiResult.data
                val isUserExist = userRepo.login.isNotBlank()
                if (isUserExist) {
                    _authFlow.update {
                        Result.Success(AuthState.Authorized(userRepo.toUser()))
                    }
                } else {
                    _authFlow.update {
                        Result.Error(UnknownException())
                    }
                    _authFlow.update { prevState }
                }
            }
        }
    }

    override fun logout() {
        _authFlow.update {
            Result.Success(
                AuthState.NotAuthorized
            )
        }
    }

    companion object {
        private const val USER_EXISTS_ERROR_STATUS = "status: 422"
    }
}