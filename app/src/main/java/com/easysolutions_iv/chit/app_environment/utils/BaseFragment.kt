package com.easysolutions_iv.chit.app_environment.utils

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding

/** Основное предназначение данного класса - предотвращать утечки памяти из-за использования DataBinding */
abstract class BaseFragment<VB : ViewBinding> : Fragment() {

    private var _viewBinding: VB? = null
    lateinit var navController: NavController

    val viewBinding: VB
        get() = requireNotNull(_viewBinding)

    /** Функция должна вернуть ViewBinding */
    abstract fun provideBinding(inflater: LayoutInflater, container: ViewGroup?): VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navController = findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _viewBinding = provideBinding(inflater, container)
        return _viewBinding?.root
    }

    override fun onDestroyView() {
        _viewBinding = null
        super.onDestroyView()
    }
}