package com.easysolutions_iv.chit.feature_common_src.models

import kotlinx.serialization.Serializable

@Serializable
data class UserInternal(
    val login: String,
    val password: String,
    val nickName: String,
    val role: String
)