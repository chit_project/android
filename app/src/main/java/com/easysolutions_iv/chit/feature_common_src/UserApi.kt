package com.easysolutions_iv.chit.feature_common_src

import com.easysolutions_iv.chit.feature_common_src.models.UserInternal
import com.easysolutions_iv.chit.feature_common_src.models.UserRepo
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.resources.*
import kotlinx.serialization.Serializable
import javax.inject.Inject

class UserApi @Inject constructor(private val client: HttpClient) {

    suspend fun getUserBy(login: String, password: String): UserRepo = client.get(GetUser()) {
        parameter("login", login)
        parameter("password", password)
    }.body()

    suspend fun postUser(user: UserInternal): UserRepo = client.post(PostUser()) {
        contentType(ContentType.Application.Json)
        setBody(user)
    }.body()
}

@Serializable
@Resource("/get_user")
class GetUser

@Serializable
@Resource("/post_user")
class PostUser