package com.easysolutions_iv.chit.app_environment.di_modules.for_whatever_repositories

import com.easysolutions_iv.chit.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.plugins.resources.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import timber.log.Timber
import javax.inject.Singleton
import kotlin.time.Duration.Companion.seconds

@Module
@InstallIn(SingletonComponent::class)
object AppApiModule {

    @Singleton
    @Provides
    fun provideJson() = Json {
        coerceInputValues = true
        encodeDefaults = true
        ignoreUnknownKeys = true
        isLenient = true
    }

    @Provides
    fun providesKtorLogger() = object : Logger {
        override fun log(message: String) {
            Timber.tag("HTTP_TAG").d(message)
        }
    }

    @Singleton
    @Provides
    fun provideKtorClient(json: Json, logger: Logger) = HttpClient(OkHttp) {
        install(ContentNegotiation) { json(json) }
        install(HttpTimeout) {
            requestTimeoutMillis = 30.seconds.inWholeMilliseconds
            connectTimeoutMillis = 30.seconds.inWholeMilliseconds
            socketTimeoutMillis = 30.seconds.inWholeMilliseconds
        }
        install(Resources)
        Logging {
            this.logger = logger
            level = if (BuildConfig.DEBUG) LogLevel.ALL else LogLevel.NONE
        }
        defaultRequest {
            url("http://82.146.40.21:8070")
        }
    }
}