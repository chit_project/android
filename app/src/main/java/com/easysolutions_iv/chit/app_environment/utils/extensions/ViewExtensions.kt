package com.easysolutions_iv.chit.app_environment.utils.extensions

import android.util.TypedValue
import android.view.View
import androidx.appcompat.content.res.AppCompatResources

/** Установка анимации при клике */
fun View.setRippleOnClick() {
    val outValue = TypedValue()
    context.theme.resolveAttribute(android.R.attr.selectableItemBackground, outValue, true)
    foreground = AppCompatResources.getDrawable(context, outValue.resourceId)
    isClickable = true
    isSelected = true
}

fun View.dpToPxInt(dp: Int): Int = (dp * resources.displayMetrics.density).toInt()
