package com.easysolutions_iv.chit.features.auth.repository

import com.easysolutions_iv.chit.app_environment.utils.Result
import com.easysolutions_iv.chit.feature_common_src.models.UserInternal
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import kotlinx.coroutines.flow.Flow

interface AuthRepository {

    val authFlow: Flow<Result<AuthState>>

    suspend fun tryToLogin(login: String, password: String)

    suspend fun tryToRegister(userRepoInternal: UserInternal)

    fun logout()
}