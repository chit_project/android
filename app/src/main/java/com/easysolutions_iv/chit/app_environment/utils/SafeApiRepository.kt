package com.easysolutions_iv.chit.app_environment.utils

import io.ktor.client.plugins.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.onSubscription
import kotlinx.coroutines.flow.transform
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SafeApiRepository @Inject constructor() {

    private val httpExceptionReport = MutableSharedFlow<Result.Error?>()

    suspend fun <T> safeApiCall(
        isBackgroundApiCall: Boolean = false,
        apiCall: suspend () -> T
    ): Result<T> {
        return try {
            Result.Success(apiCall.invoke())
        } catch (throwable: Throwable) {

            // если корутину отменили - нет необходимости обрабатывать какой то результат
            // если нет кастомного обработчика при запуске корутины - этот Exception игнорируется
            // Ошибка пробросится в место вызова корутины и ничего не произойдет
            // https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-coroutine-exception-handler/
            if (throwable is CancellationException) throw throwable

            val error = when (throwable) {

                // в т.ч. ошибки 300..599 прилетают сюда
                is ResponseException -> {
                    Timber.e(throwable, "HttpError")
                    Result.Error(Exception(throwable), isBackgroundApiCall = isBackgroundApiCall)
                }

                else -> {
                    Timber.e(throwable, "Not HttpException error by request to the server.")
                    Result.Error(throwable as Exception, isBackgroundApiCall = isBackgroundApiCall)
                }
            }

            httpExceptionReport.emit(error)

            error
        }
    }

    /** Сообщает о HTTP-ошибках */
    fun observeError(): Flow<Result.Error?> = httpExceptionReport
        .onSubscription {
            // делам начальное событие для каждого подписчика, чтобы не висело ожидание
            // актуально, например, для combine оператора
            emit(null)
        }
        .transform { error ->
            emit(error)
            // если был error != null - то делаем доп событие для очистки
            // например, чтобы значение не хранилось в combine подписчика
            if (error != null) emit(null)
        }
}