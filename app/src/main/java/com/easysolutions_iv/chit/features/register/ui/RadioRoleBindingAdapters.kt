package com.easysolutions_iv.chit.features.register.ui

import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.BindingAdapter
import com.easysolutions_iv.chit.R
import com.easysolutions_iv.chit.app_environment.utils.extensions.getCompatDrawable
import com.easysolutions_iv.chit.app_environment.utils.extensions.setRippleOnClick
import com.easysolutions_iv.chit.features.register.ui.models.RoleUI

@BindingAdapter("generateRadioButtons")
fun RadioGroup.generateRadioButtons(roles: List<RoleUI>) {
    removeAllViews()
    val rbs = arrayOfNulls<RadioButton>(roles.size)
    for (i in roles.indices) {
        rbs[i] = RadioButton(context)
            .apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                buttonDrawable =
                    context.getCompatDrawable(R.drawable.radio_button_inset)

                // включить риппл-эффект на всю строку
                setRippleOnClick()

                // отключить анимацию при нажатии на RadioButton
                background = AppCompatResources.getDrawable(context, android.R.color.transparent)

                id = i
                text = roles[i].role
                isChecked = roles[i].isChecked
            }
        addView(rbs[i])
    }
}