package com.easysolutions_iv.chit.features.login.ui.view_model

data class LoginViewState(
    val isProgressbarVisible: Boolean = false
)