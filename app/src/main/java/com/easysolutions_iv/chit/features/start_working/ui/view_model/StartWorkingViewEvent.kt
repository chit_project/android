package com.easysolutions_iv.chit.features.start_working.ui.view_model

sealed class StartWorkingViewEvent {

    object Logout : StartWorkingViewEvent()
}