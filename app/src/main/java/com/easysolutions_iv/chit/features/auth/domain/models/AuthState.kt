package com.easysolutions_iv.chit.features.auth.domain.models

import com.easysolutions_iv.chit.feature_common_src.models.User

/** Описание состояния авторизованности */
sealed class AuthState {

    /** Пока НЕ определено */
    object NotDefined : AuthState()

    /** НЕ авторизован */
    object NotAuthorized : AuthState()

    /** Авторизован */
    data class Authorized(val user: User) : AuthState()
}