package com.easysolutions_iv.chit.app_environment.utils.exceptions

/** Ошибка из-за того, что такой пользователь уже зарегистрирован на сервере */
class DuplicateUserException : Exception()