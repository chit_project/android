package com.easysolutions_iv.chit.features.start_working.ui.view_model

import android.content.res.Resources
import androidx.lifecycle.*
import com.easysolutions_iv.chit.R
import com.easysolutions_iv.chit.app_environment.utils.Result
import com.easysolutions_iv.chit.app_environment.utils.SingleLiveEvent
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import com.easysolutions_iv.chit.features.start_working.domain.StartWorkingInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class StartWorkingViewModel @Inject constructor(
    private val interactor: StartWorkingInteractor,
    private val resources: Resources
) : ViewModel() {

    private val _viewEvent = SingleLiveEvent<StartWorkingViewEvent>()
    val viewEvent: LiveData<StartWorkingViewEvent> = _viewEvent

    val viewState = liveData(viewModelScope.coroutineContext) {
        emit(StartWorkingViewState())
        emitSource(createViewStateLiveData())
    }.distinctUntilChanged()

    private fun LiveDataScope<StartWorkingViewState>.createViewStateLiveData() =
        interactor.authFlow.map { authStateResult ->
            val viewState: StartWorkingViewState = requireNotNull(latestValue)
            when (authStateResult) {
                is Result.Success -> {
                    when (authStateResult.data) {
                        is AuthState.Authorized -> {
                            val greetings =
                                "${resources.getString(R.string.welcome)}, ${authStateResult.data.user.nickName}!"
                            val role = "${resources.getString(R.string.role)}: ${authStateResult.data.user.role.name}."
                            viewState.copy(
                                greetings = greetings,
                                role = role
                            )
                        }
                        else -> {
                            _viewEvent.value = StartWorkingViewEvent.Logout
                            viewState
                        }
                    }
                }
                else -> viewState
            }
        }.asLiveData(viewModelScope.coroutineContext)

    fun logout() {
        interactor.logout()
    }
}