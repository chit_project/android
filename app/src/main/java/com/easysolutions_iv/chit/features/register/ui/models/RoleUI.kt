package com.easysolutions_iv.chit.features.register.ui.models

data class RoleUI(
    val role: String,
    val isChecked: Boolean
)
