package com.easysolutions_iv.chit.app_environment.utils.exceptions

/** Не известное серверу сочетание логина и пароля */
class WrongCredentialsException : Exception()