package com.easysolutions_iv.chit.features.start_working.domain

import com.easysolutions_iv.chit.features.auth.domain.AuthInteractor
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.easysolutions_iv.chit.app_environment.utils.Result

class StartWorkingInteractor @Inject constructor(
    private val authInteractor: AuthInteractor
) {

    val authFlow: Flow<Result<AuthState>>
        get() = authInteractor.authFlow

    fun logout() {
        authInteractor.logout()
    }
}