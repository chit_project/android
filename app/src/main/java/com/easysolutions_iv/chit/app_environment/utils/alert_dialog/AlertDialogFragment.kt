package com.easysolutions_iv.chit.app_environment.utils.alert_dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.easysolutions_iv.chit.databinding.DialogAlertBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import timber.log.Timber
import java.util.*

class AlertDialogFragment : DialogFragment() {

    private lateinit var alertDialogViewState: AlertDialogState
    private var binding: DialogAlertBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        alertDialogViewState =
            arguments?.getParcelable(ARGUMENTS) as AlertDialogState?
                ?: AlertDialogState(fragmentTag = UUID.randomUUID().toString())
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        activity?.let { activity ->
            binding = DialogAlertBinding.inflate(activity.layoutInflater)

            // настройка визуальной части
            binding?.apply {
                tvTitle.text = alertDialogViewState.title
                tvBody.text = alertDialogViewState.text
                tvBody.setTextIsSelectable(alertDialogViewState.isTextSelectable)
                tvBody.movementMethod = ScrollingMovementMethod()
            }

            val builder =
                MaterialAlertDialogBuilder(activity)
            builder.setView(binding?.root)

            alertDialogViewState.positiveButtonText
                .takeIf(String::isNotBlank)
                ?.let {
                    builder.setPositiveButton(it) { _, _ ->
                        alertDialogViewState.alertDialogClickListener?.onPositiveButtonClick(
                            binding?.tvBody?.text.toString()
                        )
                    }
                }

            alertDialogViewState.negativeButtonText
                .takeIf(String::isNotBlank)
                ?.let {
                    builder.setNegativeButton(it) { _, _ ->
                        alertDialogViewState.alertDialogClickListener?.onNegativeButtonClick()
                    }
                }

            alertDialogViewState.neutralButtonText
                .takeIf(String::isNotBlank)
                ?.let {
                    builder.setNeutralButton(it) { _, _ ->
                        alertDialogViewState.alertDialogClickListener?.onNeutralButtonClick()
                    }
                }

            builder.create()
        }.apply { isCancelable = alertDialogViewState.isCancelable } ?: run {
            val throwable = IllegalStateException("Activity cannot be null")
            Timber.e(throwable)
            throw throwable
        }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        binding = null
    }

    companion object {

        private const val ARGUMENTS = "ARGUMENTS"

        fun show(fragmentManager: FragmentManager, alertDialogState: AlertDialogState) {
            AlertDialogFragment().apply {
                arguments = bundleOf(ARGUMENTS to alertDialogState)
            }.show(fragmentManager, alertDialogState.fragmentTag)
        }
    }
}