package com.easysolutions_iv.chit.features.auth.di

import com.easysolutions_iv.chit.features.auth.repository.AuthRepository
import com.easysolutions_iv.chit.features.auth.repository.AuthRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface AuthBindsModule {

    @Singleton
    @Binds
    fun provideAuthRepository(repositoryImpl: AuthRepositoryImpl): AuthRepository
}