package com.easysolutions_iv.chit.app_environment.utils.alert_dialog

/** Интерфейс для работы с диалогами */
interface AlertDialogManager {
    fun showAlertDialog(dialogState: AlertDialogState)
    fun hideDialogByTag(fragmentTag: String)
}