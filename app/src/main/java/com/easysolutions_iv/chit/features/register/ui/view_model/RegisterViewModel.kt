package com.easysolutions_iv.chit.features.register.ui.view_model

import android.content.res.Resources
import android.text.Editable
import android.text.TextWatcher
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.lifecycle.*
import com.easysolutions_iv.chit.R
import com.easysolutions_iv.chit.app_environment.utils.Result
import com.easysolutions_iv.chit.app_environment.utils.SingleLiveEvent
import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogState
import com.easysolutions_iv.chit.app_environment.utils.exceptions.DuplicateUserException
import com.easysolutions_iv.chit.feature_common_src.models.Role
import com.easysolutions_iv.chit.feature_common_src.models.UserInternal
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import com.easysolutions_iv.chit.features.register.domain.RegisterInteractor
import com.easysolutions_iv.chit.features.register.ui.models.RoleUI
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val interactor: RegisterInteractor,
    private val resources: Resources
) : ViewModel() {

    private val roles = MutableStateFlow(
        Role.values().map {
            val isChecked = it == Role.Developer
            RoleUI(it.name, isChecked)
        })

    private var login: String = ""
    private var password: String = ""
    private var nickName: String = ""
    private var role: String = roles.value[0].role

    private val _viewEvent = SingleLiveEvent<RegisterViewEvent>()
    val viewEvent: LiveData<RegisterViewEvent> = _viewEvent

    val viewState = liveData(viewModelScope.coroutineContext) {
        emit(RegisterViewState(roles = roles.value))
        emitSource(createViewStateLiveData())
    }.distinctUntilChanged()

    val loginWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(editable: Editable?) {
            login = editable?.toString() ?: ""
        }
    }

    val passwordWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(editable: Editable?) {
            password = editable?.toString() ?: ""
        }
    }

    val nickWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(editable: Editable?) {
            nickName = editable?.toString() ?: ""
        }
    }

    val onRadioChangeListener = { rg: RadioGroup?, position: Int ->
        val radioButton = rg?.findViewById<RadioButton>(position)
        if (radioButton?.isShown == true) {
            viewModelScope.launch {
                role = Role.values()[position].name
                val newRoles = Role.values().map {
                    val isChecked = it == Role[role]
                    RoleUI(
                        it.name,
                        isChecked
                    )
                }
                roles.update { newRoles }
            }
        }
    }

    private fun LiveDataScope<RegisterViewState>.createViewStateLiveData() =
        combine(roles, interactor.authFlow) { roles, authStateResult ->
            val viewState: RegisterViewState = requireNotNull(latestValue)
            when (authStateResult) {
                Result.Loading -> {
                    viewState.copy(isProgressbarVisible = true, roles = roles)
                }
                is Result.Error -> {
                    val text = if (authStateResult.exception is DuplicateUserException) {
                        R.string.error_duplicate_user.text()
                    } else {
                        R.string.error_network.text()
                    }
                    val state = AlertDialogState(
                        fragmentTag = "Error",
                        title = R.string.error.text(),
                        text = text,
                        positiveButtonText = resources.getString(android.R.string.ok)
                    )
                    _viewEvent.value = RegisterViewEvent.ShowAlertDialog(state)
                    viewState.copy(isProgressbarVisible = false, roles = roles)
                }
                is Result.Success -> {
                    when (authStateResult.data) {
                        is AuthState.Authorized -> {
                            _viewEvent.value = RegisterViewEvent.StartWorking
                            viewState.copy(isProgressbarVisible = false, roles = roles)
                        }
                        else -> viewState
                    }
                }
            }
        }.asLiveData(viewModelScope.coroutineContext)

    fun tryToRegister() {

        val isAllFieldsFilled = login.isNotBlank() &&
                password.isNotBlank() &&
                nickName.isNotBlank()

        if (isAllFieldsFilled) {
            viewModelScope.launch {
                interactor.tryToRegister(
                    UserInternal(
                        login = login,
                        password = password,
                        nickName = nickName,
                        role = role
                    )
                )
            }
        } else {
            val state = AlertDialogState(
                fragmentTag = "RegisterViewModel: Not all fields filled",
                title = R.string.error.text(),
                text = R.string.error_not_all_fields_filled.text(),
                positiveButtonText = resources.getString(android.R.string.ok)
            )
            _viewEvent.value = RegisterViewEvent.ShowAlertDialog(state)
        }
    }

    private fun Int.text() = resources.getString(this)
}