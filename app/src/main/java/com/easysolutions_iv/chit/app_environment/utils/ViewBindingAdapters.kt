package com.easysolutions_iv.chit.app_environment.utils

import android.text.TextWatcher
import android.widget.EditText
import androidx.databinding.BindingAdapter

@BindingAdapter("addTextInputEditTextWatcher")
fun EditText.addTextWatcher(textWatcher: TextWatcher?) {
    addTextChangedListener(textWatcher)
}