package com.easysolutions_iv.chit.features.auth.domain

import com.easysolutions_iv.chit.features.auth.repository.AuthRepository
import com.easysolutions_iv.chit.features.auth.domain.models.AuthState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.easysolutions_iv.chit.app_environment.utils.Result
import com.easysolutions_iv.chit.feature_common_src.models.UserInternal

class AuthInteractor @Inject constructor(
    private val repository: AuthRepository
) {

    val authFlow: Flow<Result<AuthState>>
        get() = repository.authFlow

    suspend fun tryToLogin(login: String, password: String) {
        repository.tryToLogin(login, password)
    }

    suspend fun tryToRegister(userRepoInternal: UserInternal) {
        repository.tryToRegister(userRepoInternal)
    }

    fun logout() {
        repository.logout()
    }
}