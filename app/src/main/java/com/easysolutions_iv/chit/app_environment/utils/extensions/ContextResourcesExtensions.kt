package com.easysolutions_iv.chit.app_environment.utils.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources

/**
 * Вспомогательные функции для получения необходимых обектов из ресурсов
 */

fun Context.getCompatDrawable(@DrawableRes drawableId: Int?): Drawable? = drawableId?.let {
    AppCompatResources.getDrawable(this, drawableId)
}