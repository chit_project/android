@file:Suppress("RemoveRedundantQualifierName")

package com.easysolutions_iv.chit.app_environment.utils

import com.easysolutions_iv.chit.app_environment.utils.Result.Success
import com.easysolutions_iv.chit.feature_common_src.models.User

/** A generic class that holds a value with its loading status */
sealed class Result<out RESULT> {

    object Loading : Result<Nothing>()

    data class Success<out DATA>(val data: DATA) : Result<DATA>()

    data class Error(
        val exception: Exception,
        val user: User? = null,
        val retry: Boolean = false,
        val errorCode: Int? = null,

        /**
         * Флаг, указывающий, был ли АПИ-запрос осуществлён из фонового процесса,
         * а не с явного экрана.
         */
        val isBackgroundApiCall: Boolean = false
    ) : Result<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            Loading -> "Loading"
        }
    }
}

/** Return [Result.Success.data] if [Result] is of type [Success], null otherwise.*/
val <T> Result<T>.data: T?
    get() = if (this is Result.Success) data else null

/** `true` if [Result] is of type [Success] & holds non-null [Success.data] */
@Suppress("unused")
val Result<*>.succeeded
    get() = this is Result.Success && data != null
