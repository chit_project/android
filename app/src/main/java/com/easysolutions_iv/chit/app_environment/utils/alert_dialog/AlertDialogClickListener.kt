package com.easysolutions_iv.chit.app_environment.utils.alert_dialog

import android.os.Parcel
import android.os.Parcelable

/** Функция для удобного создания [AlertDialogClickListener] */
inline fun AlertDialogClickListener(
    crossinline onNeutralButtonClick: () -> Unit = {},
    crossinline onNegativeButtonClick: () -> Unit = {},
    crossinline onPositiveButtonClick: (String) -> Unit = {}
) = object : AlertDialogClickListener {

    override fun onPositiveButtonClick(string: String) {
        onPositiveButtonClick(string)
    }

    override fun onNegativeButtonClick() {
        onNegativeButtonClick()
    }

    override fun onNeutralButtonClick() {
        onNeutralButtonClick()
    }

    override fun describeContents(): Int = -1
    override fun writeToParcel(dest: Parcel?, flags: Int) {}
}

/** Интерфейс предназначен для обработки события нажатий на кнопки [AlertDialogFragment] */
interface AlertDialogClickListener : Parcelable {
    fun onPositiveButtonClick(string: String = "")
    fun onNegativeButtonClick()
    fun onNeutralButtonClick()
}