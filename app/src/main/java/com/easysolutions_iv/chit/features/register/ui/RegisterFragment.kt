package com.easysolutions_iv.chit.features.register.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.easysolutions_iv.chit.R
import com.easysolutions_iv.chit.app_environment.utils.BaseFragment
import com.easysolutions_iv.chit.app_environment.utils.extensions.safeNavigate
import com.easysolutions_iv.chit.app_environment.utils.extensions.setKeyboardVisibility
import com.easysolutions_iv.chit.app_environment.utils.extensions.showAlertDialog
import com.easysolutions_iv.chit.databinding.FragmentRegisterBinding
import com.easysolutions_iv.chit.features.register.ui.view_model.RegisterViewEvent
import com.easysolutions_iv.chit.features.register.ui.view_model.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding>() {

    private val fragmentViewModel by viewModels<RegisterViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.lifecycleOwner = viewLifecycleOwner
        viewBinding.viewModel = fragmentViewModel

        viewBinding.login.setKeyboardVisibility(true)

        fragmentViewModel.viewEvent.observe(viewLifecycleOwner) { event ->
            when (event) {
                is RegisterViewEvent.ShowAlertDialog -> showAlertDialog(event.state)
                RegisterViewEvent.StartWorking -> {
                    safeNavigate(
                        R.id.registerFragment,
                        RegisterFragmentDirections.actionRegisterFragmentToStartWorkingFragment()
                    )
                }
            }
        }
    }

    override fun provideBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentRegisterBinding.inflate(inflater, container, false)
}