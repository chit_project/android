package com.easysolutions_iv.chit.features.login.ui.view_model

import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogState

sealed class LoginViewEvent {

    object Register : LoginViewEvent()

    object StartWorking : LoginViewEvent()

    data class ShowAlertDialog(val state: AlertDialogState) : LoginViewEvent()
}