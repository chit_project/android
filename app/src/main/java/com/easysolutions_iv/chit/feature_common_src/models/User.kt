package com.easysolutions_iv.chit.feature_common_src.models

data class User(
    val login: String,
    val nickName: String,
    val role: Role
)