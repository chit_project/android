package com.easysolutions_iv.chit.app_environment.utils.extensions

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.easysolutions_iv.chit.app_environment.utils.BaseFragment
import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogManager
import com.easysolutions_iv.chit.app_environment.utils.alert_dialog.AlertDialogState

fun BaseFragment<*>.showAlertDialog(dialogState: AlertDialogState) {
    (requireActivity() as? AlertDialogManager)?.showAlertDialog(dialogState)
}

fun BaseFragment<*>.hideDialogByTag(fragmentTag: String) {
    (requireActivity() as? AlertDialogManager)?.hideDialogByTag(fragmentTag)
}

/**
 * Безопасный способ навигации
 * Необходимо применять в случаях, когда возможен переход из неверного destination, например при
 * быстром двойном клике по элементу
 *
 * Для избежания подобного рода исключений:
 * Throwable.message: Navigation action/destination ru.post.postmaster:id/action_startFragment_to_select cannot be found
 * from the current destination Destination(ru.post.postmaster:id/selectFragment) label=SelectFragment class=ru.post.view_components.fragments.select.SelectFragment
 * - хотя вызов при этом происходил из StartFragment (а не из SelectFragment)
 *
 * @param currentDestinationId - id destination, откуда планируется переход
 * @param directions - action для перехода
 */
fun Fragment.safeNavigate(
    @IdRes currentDestinationId: Int,
    directions: NavDirections
) {
    with(findNavController()) {
        if (currentDestinationId == currentDestination?.id) navigate(directions)
    }
}

/** @see [Fragment.safeNavigate] */
fun Fragment.safeNavigate(
    @IdRes currentDestinationId: Int,
    @IdRes destination: Int,
    args: Bundle? = null
) {
    with(findNavController()) {
        if (currentDestinationId == currentDestination?.id) navigate(destination, args)
    }
}